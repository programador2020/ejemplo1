package ejemplo1;

public class Ejemplo1 {

    public static void main(String[] args) {
        // 1. Declaramos el usuario y la clave. Seria como el registro.
        // 2. Ingresamos usaurio y clave a verificar
        // 3. Dar mensaje de bienvenida o de error
        
        //1.
        String usuarioRegistrado = "diego89";
        String claveRegistrada = "programacion29";
        String usuario;
        String clave;
        String mensaje;
        
        //2.
        //2.1 Caso de OK: Usuario y clave coinciden
        
        // Aca se ingresa el usuario
        usuario = "diego89";
        //Aca se ingresa la clave
        clave = "programacion29";
        
//        if (usuarioRegistrado.equals(usuario) && claveRegistrada.equals(clave)) {            
//            mensaje = "Bienvenido/a";            
//        }else{
//            mensaje = "Usuario o clave incorrecta";
//        }
//        
//        System.out.println(mensaje);
        
        
        
        //2.2 Caso de error. Usuario o clave no coinciden
        //Hay que ingresar el usuario y la clave
                
        // Aca se ingresa el usuario
        usuario = "pepe";
        //Aca se ingresa la clave
        clave = "12345678";
        
        if (usuarioRegistrado.equals(usuario) && claveRegistrada.equals(clave)) {
            mensaje = "Bienvenido/a";
        }else{
            mensaje = "usuario o clave incorrecto";
        }
        System.out.println(mensaje);
        
               
    }
    
}
